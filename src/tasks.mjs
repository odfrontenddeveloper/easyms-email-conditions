import gulp from 'gulp'
import cleanCSS from 'gulp-clean-css'
import pug from 'gulp-pug'
import autoprefixer from 'gulp-autoprefixer'
import imagemin from 'gulp-imagemin'
import sass from 'gulp-sass'
import browserSync from 'browser-sync'
import plumber from 'gulp-plumber'

import {
    BUILD_PATH,
    PUG_SOURCE_PATH,
    PUG_FILES_PATH,
    SCSS_SOURCE_PATH,
    CSS_DEST_PATH,
    SCSS_FILES_PATH,
    IMAGES_SOUCE_PATH,
    IMAGES_DEST_PATH,
} from './constants.mjs'

const browserSyncUpdate = browserSync.create()

export const buildHTML = function () {
    return gulp.src(PUG_SOURCE_PATH).pipe(plumber()).pipe(pug()).pipe(gulp.dest(BUILD_PATH))
}

export const buildCSS = function () {
    return gulp
        .src(SCSS_SOURCE_PATH)
        .pipe(sass().on('error', sass.logError))
        .pipe(
            autoprefixer({
                overrideBrowserslist: ['last 2 versions'],
                cascade: false,
            })
        )
        .pipe(gulp.dest(CSS_DEST_PATH))
}

export const buildCSSwithMin = function () {
    return gulp
        .src(SCSS_SOURCE_PATH)
        .pipe(sass().on('error', sass.logError))
        .pipe(
            autoprefixer({
                overrideBrowserslist: ['last 2 versions'],
                cascade: false,
            })
        )
        .pipe(
            cleanCSS({
                level: 2,
            })
        )
        .pipe(gulp.dest(CSS_DEST_PATH))
}

export const buildImages = function () {
    return gulp.src(IMAGES_SOUCE_PATH).pipe(imagemin()).pipe(gulp.dest(IMAGES_DEST_PATH))
}

export const hotReload = function () {
    browserSyncUpdate.init({
        server: BUILD_PATH,
    })
    gulp.watch(BUILD_PATH).on('change', browserSyncUpdate.reload)

    // to do with PUG
    gulp.watch(PUG_FILES_PATH).on('change', browserSyncUpdate.reload)
    gulp.watch(PUG_FILES_PATH).on('change', function () {
        gulp.src(PUG_SOURCE_PATH).pipe(plumber()).pipe(pug()).pipe(gulp.dest(BUILD_PATH))
    })
    gulp.watch(CSS_DEST_PATH).on('change', function () {
        gulp.src(PUG_SOURCE_PATH).pipe(plumber()).pipe(pug()).pipe(gulp.dest(BUILD_PATH))
    })

    // to do with SASS
    gulp.watch(SCSS_FILES_PATH).on('change', browserSyncUpdate.reload)
    gulp.watch(SCSS_FILES_PATH).on('change', function () {
        gulp.src(SCSS_SOURCE_PATH)
            .pipe(sass().on('error', sass.logError))
            .pipe(
                autoprefixer({
                    overrideBrowserslist: ['last 2 versions'],
                    cascade: false,
                })
            )
            .pipe(gulp.dest(CSS_DEST_PATH))
    })

    // to do with IMAGES
    gulp.watch(IMAGES_DEST_PATH).on('change', browserSyncUpdate.reload)
    gulp.watch(IMAGES_DEST_PATH).on('add', browserSyncUpdate.reload)
    gulp.watch(IMAGES_DEST_PATH).on('unlink', browserSyncUpdate.reload)
}
