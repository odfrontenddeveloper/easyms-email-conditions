import gulp from 'gulp'
import { HTML_CODE, SASS_TO_CSS, SASS_TO_CSS_MIN, HOT_RELOAD, COMPRESS, DEV_MODE, PROD_MODE } from './src/constants.mjs'
import { buildHTML, buildCSS, buildCSSwithMin, buildImages, hotReload } from './src/tasks.mjs'

gulp.task(HTML_CODE, buildHTML)
gulp.task(SASS_TO_CSS, buildCSS)
gulp.task(SASS_TO_CSS_MIN, buildCSSwithMin)
gulp.task(COMPRESS, buildImages)
gulp.task(HOT_RELOAD, hotReload)

gulp.task(DEV_MODE, gulp.series(SASS_TO_CSS, HTML_CODE, gulp.parallel(COMPRESS, HOT_RELOAD)))
gulp.task(PROD_MODE, gulp.series(SASS_TO_CSS_MIN, HTML_CODE, COMPRESS))